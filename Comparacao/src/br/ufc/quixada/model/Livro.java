package br.ufc.quixada.model;

public class Livro implements Comparable<Livro>{
	private String titulo;
	private int ano;
	private int tamanho;
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getTamanho() {
		return tamanho;
	}

	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}

	public int compareTo(Livro t) {
		if(this.titulo.compareTo(t.getTitulo())>1)
			return 1;
		else if(this.titulo.compareTo(t.getTitulo())<1)
			return -1;
		else if(this.ano > t.getAno())
			return 1;
		else if(this.ano < t.getAno())
			return -1;
		else return 0;
	}
	
	public boolean equals(Object o) {
		if(!(o instanceof Livro)) return false;
		Livro l = (Livro)o;
		return this.titulo.equals(l.getTitulo()) &&
				this.ano == l.getAno();		
	}
	
	public int hashcode() {
		return (this.ano * this.ano)%1000;
	}
	
	
	
	

}
