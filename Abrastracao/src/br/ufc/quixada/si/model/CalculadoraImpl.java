package br.ufc.quixada.si.model;

public class CalculadoraImpl implements Calculadora{
	@Override
	public int soma(int a, int b) {
		return a+b;
	}
	@Override
	public int sub(int a, int b) {
		return a-b;
	}
	@Override
	public int mult(int a, int b) {
		return a*b;
	}
	@Override
	public int div(int a, int b) {
		if(b != 0) {
			return a/b;
		}else {
			return 0;
		}
	}
}
