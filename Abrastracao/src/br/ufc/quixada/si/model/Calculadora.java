package br.ufc.quixada.si.model;

public interface Calculadora {
	public int soma(int a, int b);
	public int sub(int a, int b);
	public int mult(int a, int b);
	public int div(int a, int b);
}
