package br.ufc.quixada.si.model;

public class Psicologo extends Servidor{
	private int consultasFeitas;
	
	public Psicologo(String nome, String cpf,
			double salario, int consultasFeitas) {
		super(nome, cpf, salario);
		this.consultasFeitas = consultasFeitas;
	}
	@Override  //indica a sobreescrita
	public double getBonificacao() {
		return this.consultasFeitas * 20;
	}
}
