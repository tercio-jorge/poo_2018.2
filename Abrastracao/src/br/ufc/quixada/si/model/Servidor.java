package br.ufc.quixada.si.model;

public abstract class Servidor {
	private String nome;
	private String cpf;
	protected double salario;
	
	public Servidor(String nome, String cpf,
			double salario) {
		this.nome = nome;
		this.cpf = cpf;
		this.salario = salario;
	}
	
	public abstract double getBonificacao();
	
}
