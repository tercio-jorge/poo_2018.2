package br.ufc.quixada.cc.model;

public class Psicologo extends Servidor {
	private int quantidadeConsultas;
	
	public Psicologo(String nome, String cpf,
			double salario, int quantidadeConsultas) {
		super(nome, cpf, salario);
		this.quantidadeConsultas = quantidadeConsultas;
	}
	
	@Override
	public double getBonificacao() {
		return this.quantidadeConsultas * 20;
	}
	
}
