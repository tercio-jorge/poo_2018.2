package br.ufc.quixada.cc.model;

public class STA extends Servidor {
	public STA(String nome, String cpf,
			double salario) {
		super(nome, cpf, salario);
	}
	
	@Override
	public double getBonificacao() {
		return super.salario* 0.1;
	}
}
