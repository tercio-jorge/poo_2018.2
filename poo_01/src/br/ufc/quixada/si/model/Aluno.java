package br.ufc.quixada.si.model;

public class Aluno {
	private String nome;
	private String curso;
	private int idade;
	private float ira;
	
	public Aluno() {
		
	}
	
	public Aluno(String nome, String curso, int idade, float ira) {
		this.nome = nome;
		this.curso = curso;
		this.idade = idade;
		this.ira = ira;
	}
	
	//Criando get e set
	
	public String getNome() {
		return this.nome;
	}
	//set que verifica uma possivel string de conexao
	public void setNome(String nome) {
		if(nome.startsWith("https://") || nome.startsWith("ssh:")) {
			System.out.println("String maliciosa detectada");
		}else {
			this.nome = nome;
		}
		
	}
		
	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public float getIra() {
		return ira;
	}

	public void setIra(float ira) {
		this.ira = ira;
	}

	public void estudar(String materia) {
		System.out.println(this.nome+" esta estudando "+materia);
	}
	
	public void matricular(String disciplina) {
		System.out.println(this.nome+" se matriculou em "+disciplina);
	}
	
	public String toString() {
		String modelo = "";
		modelo = "O nome do aluno: "+this.nome+"\n"+
		"A idade do aluno: "+this.idade+"\n"+
				"O curso do aluno: "+this.curso+"\n"+
		"O ira do aluno: "+this.ira;
		return modelo;
	}
	
	
	
	
	
}
