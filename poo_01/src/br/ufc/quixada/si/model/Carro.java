package br.ufc.quixada.si.model;

public class Carro {
	//Atributos ou caracteristicas
	public String marca;
	public int ano;
	public double velocidade;
	public static int numeroDeCarro;
	
	//Metodos ou ações
		
	public Carro() {
		
	}
	public Carro(double vel, String mar, int ano) {
		this.ano = ano;
		this.velocidade = vel;
		this.marca = mar;
	}
	
	public static int contarCarrosDeUmAno(Carro vetor[],
			int Ano) {
		int contador = 0;
		for(Carro c : vetor) {
			if(c.ano == Ano) contador++;
		}
				
		return contador;
	}
	
	public static void imprimeNumeroDeCarros() {
		System.out.println("Numero de carros: "+numeroDeCarro);
	}
	
	public void acelerar() {
		this.velocidade +=10;
	}
	public void parar () {
		this.velocidade = 0;
	}
	public void frear() {
		this.velocidade -= 10;
	}

}
