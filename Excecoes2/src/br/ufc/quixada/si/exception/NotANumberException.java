package br.ufc.quixada.si.exception;

public class NotANumberException extends Exception{
	public NotANumberException() {}
	public NotANumberException(String message) {
		super(message);
	}
}
