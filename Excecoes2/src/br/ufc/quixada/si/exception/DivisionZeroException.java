package br.ufc.quixada.si.exception;

public class DivisionZeroException 
extends Exception {
	public DivisionZeroException() {}
	public DivisionZeroException
	(String message) {
		super(message);
	}
}
