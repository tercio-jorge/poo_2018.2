package br.ufc.quixada.si.exception;

public class NotAOperationException extends Exception {
	public NotAOperationException() {}
	public NotAOperationException(String message) {
		super(message);
	}
}
