package br.ufc.quixada.si.model;

import br.ufc.quixada.si.exception.DivisionZeroException;

public class Calculadora {
	public int soma(int a, int b) {
		return a+b;
	}
	public int subtracao(int a, int b) {
		return a-b;
	}
	public int multiplicao(int a, int b) {
		return a*b;
	}
	public int divisao(int a, int b)
	throws DivisionZeroException{
		if(b==0) {
			throw new DivisionZeroException
			("Tentou fazer divisão por zero!");
		}else {
			return a/b;
		}
		
	}
}
