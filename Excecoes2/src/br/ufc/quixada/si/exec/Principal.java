package br.ufc.quixada.si.exec;

import java.util.Scanner;

import br.ufc.quixada.si.exception.DivisionZeroException;
import br.ufc.quixada.si.model.Calculadora;

public class Principal {
	public static Scanner scan = new Scanner(System.in);
	public static void main(String[] args) throws DivisionZeroException {
		while(true) {
			System.out.println("Digite o numerador: ");
			int a = scan.nextInt();
			System.out.println("Digite um denominador: ");
			int b = scan.nextInt();
			Calculadora calc = new Calculadora();
			try {
				calc.divisao(a, b);
			} catch (DivisionZeroException e) {
				System.out.println(e.getMessage());
			}
			//calc.divisao(a, b);
		}
	}
}
