package br.quixada.ufc.model;

public class Time {
	private String nome;
	private Jogador[] jogadores;
	private int contador  = 0;
	public Time() {
		this.jogadores = new Jogador[12];
	}
	public Time(String nome) {
		this.jogadores = new Jogador[12];
		this.nome = nome;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void addJogador(Jogador a) {
		this.jogadores[contador] = a;
		contador++;
	}
		
}
