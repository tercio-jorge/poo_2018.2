package br.quixada.ufc.model;

public class Jogador {

	private String nome;
	private String cpf;

	public Jogador(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	public Jogador() {
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCPF(String cpf) {
		this.cpf = cpf;
	}
	
	public String getCpf() {
		return this.cpf;
	}
}

