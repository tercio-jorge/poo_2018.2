package br.quixada.ufc.exec;

import br.quixada.ufc.model.Jogador;
import br.quixada.ufc.model.Partida;
import br.quixada.ufc.model.Time;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Jogador jog1 = new Jogador("1", "1");
		Jogador jog2 = new Jogador("2", "2");
		Jogador jog3 = new Jogador("3", "3");
		Jogador jog4 = new Jogador("4", "4");
		Jogador jog5 = new Jogador("5", "5");
		Jogador jog7 = new Jogador("7", "7");
		Jogador jog6 = new Jogador("6", "6");
		Jogador jog8 = new Jogador("8", "8");
		Jogador jog9 = new Jogador("nove", "nove");
		Jogador jog10 = new Jogador("dez", "dez");
		Jogador jog11 = new Jogador("onze", "11");
		Jogador jog12 = new Jogador("12", "12");
		Time time1 = new Time("flamengo" );
		Time time2 = new Time("curintias");
		time1.addJogador(jog1);
		time1.addJogador(jog2);
		time1.addJogador(jog3);
		time1.addJogador(jog4);
		time1.addJogador(jog5);
		time2.addJogador(jog1);
		time2.addJogador(jog2);
		time2.addJogador(jog3);
		time2.addJogador(jog4);
		time2.addJogador(jog5);
		Partida p1 = new Partida();
		p1.gerarResultado(time1,time2);
		
	}
}
