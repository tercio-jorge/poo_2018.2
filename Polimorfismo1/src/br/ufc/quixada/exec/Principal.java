package br.ufc.quixada.exec;

import br.ufc.quixada.si.model.Aluno;
import br.ufc.quixada.si.model.Aviao;
import br.ufc.quixada.si.model.Barco;
import br.ufc.quixada.si.model.Brinquedo;
import br.ufc.quixada.si.model.Calculadora;
import br.ufc.quixada.si.model.Carro;

public class Principal {
	public static void main(String[] args) {
		System.out.println(Calculadora.soma(1, 2));
		System.out.println(Calculadora.soma(3,4,5));
		Calculadora.soma(1.0,4.0);
		Aluno a1 = new Aluno();
		Aluno a2 = new Aluno("Aluno");
		Aluno a3 = new Aluno("Aluno1", "UECE");
		Aluno a4 = new Aluno("Aluno2", "IFCE", 23);
		Brinquedo b = new Carro();
		b.mover();
		Brinquedo c = new Barco();
		c.mover();
		Brinquedo d = new Aviao();
		d.mover();
		Brinquedo a = new Brinquedo();
		a.mover();
		
	}
}
