package br.ufc.quixada.si.model;

public class Brinquedo {
	private String nome;
	private double velocidade;
	private double aceleracao;
	
	public Brinquedo() {
		
	}
	
	public Brinquedo(String nome,
			double velocidade,
			double aceleracao) {
		this.nome = nome;
		this.velocidade = velocidade;
		this.aceleracao = aceleracao;
	}
	
	public void mover() {
		System.out.println("O brinquedo está se movendo");
	}
	
	public void velocidade(int vel) {
		
	}
	
	public void velocidade(double vel) {
		
	}
	
	public void velocidade(float vel, double ac) {
		
	}
	
}
