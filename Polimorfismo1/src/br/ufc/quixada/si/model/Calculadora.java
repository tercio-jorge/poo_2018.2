package br.ufc.quixada.si.model;

public class Calculadora {
	public static int soma(int a, int b) {
		return a+b;
	}
	
	public static int soma(int a, int b, int c) {
		return a+b+c;
	}
	
	public static double soma(double a, double b) {
		return a+b;
	}
}
