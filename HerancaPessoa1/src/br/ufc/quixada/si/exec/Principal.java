package br.ufc.quixada.si.exec;

import java.time.LocalDate;

import br.ufc.quixada.si.model.Aluno;
import br.ufc.quixada.si.model.ChefeDepartamento;
import br.ufc.quixada.si.model.Funcionario;
import br.ufc.quixada.si.model.Pessoa;

public class Principal {

	public static void main(String[] args) {
		LocalDate ld = LocalDate.of(2000, 10, 11);
		Pessoa p = new Pessoa("Pessoa","123.234.255-34",ld);
		Funcionario f = new Funcionario("Funcionario", "234.233.235-34", ld, "1243", ld, 800);
		ChefeDepartamento c = new ChefeDepartamento("Chefe", "124.234.234-55", ld, "1345", ld, 2400, "TI", ld, 500);
		Aluno a = new Aluno("Aluno", "135.345.232-55", ld, "333044");
		
		p.mostrarPessoa();
		f.mostrarFuncionario();
		c.mostrarChefe();
		a.mostrarAluno();
		
		
		
	}

}
