package br.ufc.quixada.si.model;

import java.time.LocalDate;

public class Pessoa  {
	private String nome;
	private String cpf;
	private LocalDate dataNasc;
	
	public Pessoa() {
		
	}
	
	public Pessoa(String nome, 
			String cpf, 
			LocalDate dataNasc) {
		this.nome = nome;
		this.cpf = cpf;
		this.dataNasc = dataNasc;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}
	
	public void mostrarPessoa() {
		System.out.println("Print de Pessoa");
		System.out.println("Nome: "+this.nome);
		System.out.println("Cpf: "+this.cpf);
		System.out.println("Data Nasc: "+this.dataNasc);
	}
	
	
}
