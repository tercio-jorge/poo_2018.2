package br.ufc.quixada.si.model;

import java.time.LocalDate;

public class Funcionario extends Pessoa {
	private String matricula;
	private LocalDate dataAdmissao;
	private double salario;
	
	public Funcionario() {
		super();
	}
	
	public Funcionario(String nome, 
			String cpf, 
			LocalDate dataNasc,
			String matricula, 
			LocalDate dataAdmissao,
			double salario) {
		super(nome, cpf, dataNasc);
		this.matricula = matricula;
		this.dataAdmissao = dataAdmissao;
		this.salario = salario;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	public void mostrarFuncionario() {
		super.mostrarPessoa();
		System.out.println("Print do Funcionario");
		System.out.println("Matricula: "+this.matricula);
		System.out.println("Data Admissao: "+this.dataAdmissao);
		System.out.println("Salario: "+this.salario);
	}
	
}
