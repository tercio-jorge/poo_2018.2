package br.ufc.quixada.cc.exec;

import java.time.LocalDate;

import br.ufc.quixada.cc.model.Aluno;
import br.ufc.quixada.cc.model.ChefeDepartamento;
import br.ufc.quixada.cc.model.Funcionario;
import br.ufc.quixada.cc.model.Pessoa;

public class Principal {
	public static void main(String[] args) {
		LocalDate ld = LocalDate.of(2000, 11, 5);
		Pessoa p = new Pessoa("Pessoa","124.333.222-34", ld);
		Funcionario f = new Funcionario("Funcionario", "134.233.444-55", ld, "13445", ld, 800);
		ChefeDepartamento c = new ChefeDepartamento("Chefe", "124.323.343-55", ld, "234", ld, 799, "RH", ld, 3500);
		Aluno a = new Aluno("Aluno", "124.343.345-22", ld, "1132424");
		
		p.mostrarPessoa();
		f.mostrarFuncionario();
		c.mostrarChefe();
		a.mostrarAluno();
		
		
		
	}
}
