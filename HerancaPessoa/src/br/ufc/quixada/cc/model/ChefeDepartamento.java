package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class ChefeDepartamento extends Funcionario {
	private String departamento;
	private LocalDate dataPromocao;
	private double gratificacao;
	
	public ChefeDepartamento() {
		super();
	}
	
	public ChefeDepartamento(String nome,
			String cpf, 
			LocalDate dataNasc,
			String matricula, 
			LocalDate dataAdmissao, 
			double salario,
			String departamento,
			LocalDate dataPromocao, 
			double gratificacao) {
		super(nome, cpf, dataNasc, 
				matricula, dataAdmissao,
				salario);
		this.departamento = departamento;
		this.dataPromocao = dataPromocao;
		this.gratificacao = gratificacao;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public LocalDate getDataPromocao() {
		return dataPromocao;
	}

	public void setDataPromocao(LocalDate dataPromocao) {
		this.dataPromocao = dataPromocao;
	}

	public double getGratificacao() {
		return gratificacao;
	}

	public void setGratificacao(double gratificacao) {
		this.gratificacao = gratificacao;
	}
	
	public void mostrarChefe() {
		super.mostrarFuncionario();
		System.out.println("Imprimindo Chefe");
		System.out.println("Departamento: "+this.departamento);
		System.out.println("Data de Promoção: "+this.dataPromocao);
		System.out.println("Gratifição: "+this.gratificacao);
	}
	
}
