package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class Aluno extends Pessoa{
	private String matricula;
	
	public Aluno() {
		super();
	}
	
	public Aluno(String nome,
			String cpf, 
			LocalDate dataNasc,
			String matricula) {
		super(nome, cpf, dataNasc);
		this.matricula = matricula;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public void mostrarAluno() {
		super.mostrarPessoa();
		System.out.println("Imprimindo Aluno");
		System.out.println("Matricula: "+this.matricula);
	}
	
	
}
