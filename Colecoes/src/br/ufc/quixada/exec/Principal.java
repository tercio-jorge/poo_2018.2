package br.ufc.quixada.exec;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Principal {

	public static void main(String[] args) {
		
		Map<Integer, String> mapa = new HashMap<>();
		mapa.put(200, "UFC");
		mapa.put(100, "IFCE");
		mapa.put(50, "UECE");
		mapa.put(200, "UFMG");
		
		String a = mapa.get(200);
		System.out.println(a);
		
		for(Entry<Integer,String> e : mapa.entrySet()) {
			System.out.println("Chave: "+e.getKey());
			System.out.println("Valor: "+e.getValue());
			System.out.println(e);
		}

	}
}
